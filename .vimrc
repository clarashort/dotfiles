set autoindent
set expandtab
set tabstop=4
set shiftwidth=4

set number
set ruler
set hlsearch

set modelines=5
syntax on
autocmd BufNewFile,BufRead *.p8 set syntax=lua
autocmd BufWritePre * :%s/\s\+$//e
